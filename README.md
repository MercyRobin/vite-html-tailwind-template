# Vite-HTML-TailwindCSS-Template

Simple Vite, HTML & TailwindCSS Template for faster development.

## Getting Started

### Clone to Local
Clone this repository to your local machine.

    git clone https://gitlab.com/er-arunkumar/vite-html-tailwind-template.git

### Installation
Navigate to the template folder and run the following command in your terminal:

    cd tailwindcss-template
    
    npm install

## Usage

### Development
To start the development server, run:

    npm run dev


Visit http://localhost:5173 in your browser to view the project.

### Building
To build the application for production, run:

    npm run build

The generated files will be located in the `dist` directory, ready to be served.

### Previewing
To preview the build files, run:

    npm run preview

## License
This project is licensed under the MIT License.

## Author

* Arunkumar    

